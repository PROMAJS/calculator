package calculator;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CalculatorTest {

    Calculations calculations = new Calculations();
    List<OperationWithValue> testList = new ArrayList<>();

    @Test
    public void shouldCountResult() {
        testList.add(new OperationWithValue("multiply", 5));
        testList.add(new OperationWithValue("divide", 4));
        testList.add(new OperationWithValue("apply", 10));

        Assert.assertEquals(12, calculations.countResult(testList, 10));

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotDivideByZero() {
        testList = new ArrayList<>();
        testList.add(new OperationWithValue("multiply", 5));
        testList.add(new OperationWithValue("divide", 0));
        testList.add(new OperationWithValue("apply", 10));
        Assert.assertEquals(10, calculations.countResult(testList, 10));

    }
}