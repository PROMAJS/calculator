package calculator;

public class OperationWithValue {

    private String operation;
    private int value;

    public String getOperation() {
        return operation;
    }

    public int getValue() {
        return value;
    }


    public OperationWithValue(String operation, int value) {
        this.operation = operation;
        this.value = value;
    }


}
