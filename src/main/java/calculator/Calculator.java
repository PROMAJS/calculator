package calculator;

import input.DataInput;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class Calculator {
    private String operation;
    private int value;
    private static DataInput dataInput = new DataInput();
    private static List<OperationWithValue> listOfData = new ArrayList<>();
    Calculations calculations = new Calculations();

    public void getResult() {

        BufferedReader reader = dataInput.getInput();

        try {
            reader.lines().forEach((String linia) -> {
                        if (!linia.equals("")) {                                           // Zabezpieczenie przez ewentualnym pustymi liniami w pliku
                            String[] resultOfSplit = linia.split(" ", 2);      // Podzial wczytanej linii na : działanie i wartość
                            operation = resultOfSplit[0];
                            value = Integer.parseInt(resultOfSplit[1]);
                            listOfData.add(new OperationWithValue(operation, value));      // Dodanie danych do listy
                            if (resultOfSplit[0].equals("apply")) {
                                try {
                                    System.out.println(calculations.countResult(listOfData, value)); /// Wylicza result po znalezieniu "apply" w klasie Calculations
                                    listOfData.clear();
                                } catch (IllegalArgumentException e) {                      // Wyjątek w przypadku gdy wykonywane jest dzielenie przez 0
                                    System.out.println("Dividing by 0, fix your data");
                                    listOfData.clear();
                                }
                            }
                        }
                    }
            );
            reader.close();                                                                 // Zamykanie pliku
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


