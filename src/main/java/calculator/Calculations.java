package calculator;

import calculator.OperationWithValue;

import java.util.List;

public class Calculations {

    public int countResult(List<OperationWithValue> list, int valueOfApply) throws IllegalArgumentException {
        int result = valueOfApply;
        String operation;
        int value;

        if (list.size() == 1) {    // Sytuacja gdy jedyną czynością do wykonania jest "apply" tj. brak działan poprzedzających
            return valueOfApply;
        }

        for (int i = 0; i < list.size() - 1; i++) {

            if (checkDivideByZero(list, i)) {           // Sprawdzenie czy nie żadane jest dzielenie przez 0
                throw new IllegalArgumentException();
            }

            operation = list.get(i).getOperation();
            value = list.get(i).getValue();

            switch (operation) {
                    case "add":
                        result = result + value;
                        break;
                    case "substract":
                        result = result - value;
                        break;
                    case "multiply":
                        result = result * value;
                        break;
                    case "divide":
                        result = result / value;
                        break;
                }
            }
        return result;
    }

    private boolean checkDivideByZero(List<OperationWithValue> list, int number) {  // Metoda sprawdzająca dzielenie przez 0

        if (list.get(number).getOperation().equals("divide") && list.get(number).getValue() == 0) {
            return true;
        }
        return false;
    }
}
