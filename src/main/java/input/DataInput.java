package input;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


public class DataInput {

    public BufferedReader getInput() {
        try {
            InputStream is = Files.newInputStream(Paths.get("src/main/calculations.txt"));
            InputStreamReader inputStreamReader = new InputStreamReader(is);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            return reader;
        } catch (IOException e) {
            e.printStackTrace();
        }
       return null;
    }
}
