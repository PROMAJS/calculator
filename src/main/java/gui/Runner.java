package gui;

import calculator.Calculator;

public class Runner {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.getResult();
    }
}
